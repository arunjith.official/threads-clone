import { sidebarLinks } from "@/constants";
import Image from "next/image";
import Link from "next/link";

function LeftSidebar() {
    return (
        <section className="costom-scrollbar leftsidebar">
            <div className="flex w-full flex-1 flex-col gap-6">
                {sidebarLinks.map((link) => (
                        
                        <Link href={link.route} key={link.label} className="leftsidebar_link">
                        <a>
                            <Image src={link.imgURL} alt={link.label} width={24} height={24} />
                            <p className="text-light-1 max-lg:hidden">{link.label}</p>
                        </a>
                    </Link>
                    
                    ))}
            </div>
        </section>
    )
}

export default LeftSidebar;